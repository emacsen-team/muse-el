muse-el (3.20+git20240209.8710add+dfsg2-1) unstable; urgency=medium

  * Repack source removing files no longer in upstream
  * Drop handling obsolete files in d/rules and make muse-el build
    reproducible (Closes: #889849)
  * Add d/gbp.conf

 -- Xiyue Deng <manphiz@gmail.com>  Mon, 21 Oct 2024 22:12:56 -0700

muse-el (3.20+git20240209.8710add+dfsg-1) unstable; urgency=medium

  * Switch upstream to elpa and sync to latest upstream head (8710add).
  * Drop patches applied upstream
  * Drop comments in d/watch now that we are tracking elpa (Closes: #1051247)
  * Update year and add Upstream-Contact in d/copyright
  * Drop unused lintian overrides.
  * Declare Standards-Version 4.7.0; no change needed

 -- Xiyue Deng <manphiz@gmail.com>  Sun, 04 Aug 2024 04:01:37 -0700

muse-el (3.20+dfsg-9) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:54:15 +0900

muse-el (3.20+dfsg-8) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + elpa-muse: Drop versioned constraint on emacs in Recommends.
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Move source package lintian overrides to debian/source.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

  [ Xiyue Deng ]
  * Add patch 0006-Migrate-away-from-deprecated-assoc.el.patch to fix
    build with Emacs 29.1.  Closes: #1042911.
  * Set package as team maintained and add myself as an uploader.
    Closes: #1016558.
  * Drop match pattern for privacy-breach-generic so as to match the
    included URLs.
  * Add lintian override for debian-news-entry-has-unknown-version to
    workaround #1021502.
  * Drop section referencing non-existing file in debian/copyright to fix
    lintian warning superfluous-file-pattern.
  * Drop lintian override of wrong-section-according-to-package-name.
  * Save and restore lisp/muse-autoloads.el to prevent it from changing
    when build-twice-in-a-row.  Closes: #1048114.

  [ Xiyue Deng & Nicholas D Steeves ]
  * Correct watch file so that it tracks the official GNU project on Savannah,
    rather than the fork on Github.  Additionally, start tracking all upstream
    activity (not just tags).

 -- Xiyue Deng <manphiz@gmail.com>  Thu, 07 Sep 2023 21:32:02 -0700

muse-el (3.20+dfsg-7) unstable; urgency=medium

  * QA upload

  [ Nicholas D Steeves ]
  * Update my email address.

  [ David Bremner ]
  * Drop QuickStart.pdf from installed documentation. (Closes: #1016335).
  * Drop docbase support (Quickstart.pdf was the only installed file)

 -- David Bremner <bremner@debian.org>  Sun, 11 Sep 2022 09:19:05 -0300

muse-el (3.20+dfsg-6) unstable; urgency=medium

  * Rebuild with dh-elpa 2.0.
  * Migrate to debhelper-compat 13.
  * Add Rules-Requires-Root: no.
  * Update my copyright years.
  * Add 0005-convert-a-muse-init.el-example-to-UTF-8.patch.
  * Declare Standards-Version: 4.5.1 (no changes required).

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 08 Dec 2020 06:48:39 -0500

muse-el (3.20+dfsg-5) unstable; urgency=medium

  * Update Maintainer to use the Debian Emacsen Team's new email address.
  * Uncapitalise short description, and fix ancient typos.
  * Use secure URL for copyright-format.
  * Revert section change to lisp.  Editors was correct, and the Debian
    archive currently classifies this package as such.
  * Add lintian override for elpa-muse' wrong-section-according-to-package-name
  * Switch Vcs links from alioth to salsa.
  * Switch to debhelper-compat 12 and drop debian/compat.
  * Drop the muse-el dummy transitional package.
  * Add Provides: muse-el to elpa-muse, for the benefit of users who only
    know this package by that name.
  * Update my copyright years.
  * Change Build-Depends-Indep: emacs25-nox to emacs-nox (unversioning).
  * debian/rules:
    - Delete unused remnants of boilerplate.
    - Override dh_auto_install, which is now installing files to
      debian/elpa-muse/usr/local, to do nothing.
  * Update Breaks and Replaces for 3.20+dfsg-5.
  * Drop emacs25 from Enhances, because it doesn't exist in Buster.
  * Declare Standards-Version: 4.4.0. (No additional changes needed)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 08 Jul 2019 14:07:43 -0400

muse-el (3.20+dfsg-4) unstable; urgency=medium

  * Declare Standards-Version: 4.1.3. (No additional changes needed)
  * Switch to debhelper 11.
  * Declare compat level 11.
  * Update packaging to current pkg-emacsen standards:
    - Change from Section: editors to Section: lisp.
    - Drop emacs24 everywhere.
    - Drop unnecessary emacsen-common dependency.
    - Drop Built-Using.
  * Drop depends on emacs-goodies-el, and depend on elpa-htmlize instead.
  * debian/control: Use my preferred formatting.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 07 Jan 2018 18:14:58 -0500

muse-el (3.20+dfsg-3) unstable; urgency=medium

  * Upload to unstable.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 19 Jun 2017 19:34:13 -0400

muse-el (3.20+dfsg-2) experimental; urgency=medium

  * debian/COPYING.emails: Add confirmation emails from all previous
    contributors.
  * debian/copyright:
    - Add license for debian/*
    - Primary GPL3+ section can finally be simplified; simplify it.
  * debian/control: Make dependencies comply with Debian Emacs Policy.
    - unversioned dh-elpa
    - Drop emacs-nox from Build-Depends-Indep.
    - Add emacsen-common to Depends.
  * debian/elpa-muse.install: Fix creation of redundant subdir under
    contrib.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 09 Jun 2017 19:10:22 -0400

muse-el (3.20+dfsg-1) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * New Maintainer. (Closes: #715466)
  * Repackage using dh-elpa:
    - debian/control: add dh-elpa build dependency.
    - Rename the binary package to elpa-muse.
    - 'muse-el' is now a dummy transitional package.
    - debian/rules: Start from scratch, because it's a complete rewrite.
    - Drop debian/emacsen-* and debian/muse-el.dirs.
    - Add update_dir_struct.diff patch for upstream README to reflect
      the new locations various files are installed to.
    - Update NEWS and README.Debian with new elpa-muse locations, and remove
      asterisks from NEWS.
  * Declare source format 3.0 (quilt) non-native.
  * debian/control:
    - Change section to editors.
    - Change priority to optional.
    - Bump required debhelper to >=10.
    - Update Emacs build dependency to emacs-nox | emacs25 | emacs24.
    - Add texlive-latex-base dependency recommendation, which is needed for
      many functions, including TeX import/export and PDF export targets.
    - Add homepage, and Vcs links.
    - Bump standards version to 3.9.8.
      Various changes elsewhere in this changelog entry.
  * Register Quickstart.pdf with doc-base.
  * Switch to dh compat 10.
  * Add patch fix_privacy_breaches.diff; this removes CSS validation links and
    prevents the download of favicon. (Closes: #775885)
  * Add patch no_texi.diff to disable non-DFSG compliant documentation.
  * Install docs and examples using dh_install* tools.
  * debian/rules:
    - Override dh_auto_build to build examples and autoloads.
    - Override dh_installchangelogs to install upstream changelog series.
  * Install images used by the QuickStart.muse example to
    doc/elpa-muse-version/examples. (Closes: #720121)
  * Add patch use_see_not_open.diff to use Debian's "see" command rather
    than "open", the command a popular commercial OS uses to automatically
    choose the correct helper program to open files.  Thank you to Kevin Ryde,
    who suggested this change. (Closes: #720126)
  * Add watch file.
  * Add elpa-muse.lintian-overrides and source.lintian-overrides.  Refer
    to comments in these files for the rationale behind this.
  * Add muse-el.maintscript to remove obsolete 50muse-el.el.

  [ Sean Whitton ]
  * Add dependency on emacs-goodies-el.
    htmlize.el is needed for postinst to bytecompile htmlize-hack.el

  [ Nicholas D Steeves & Sean Whitton ]
  * debian/copyright:
    - Rewrite to use machine-readable format 1.0.
    - Fix misattributed source; timestamps found in the source code of our
      muse-el_3.20+dfsg.orig.tar.gz do not match those of the tarball
      provided by http://download.gna.org, but they match what
      git://repo.or.cz provides; consequently, this git repo must be cited
      as the verifiable origin of muse-el_3.20+dfsg.orig.tar.gz.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 15 Jan 2017 22:05:09 -0700

muse-el (3.20+dfsg-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove unused and non DFSG-compliant texi/muse.texi from source.
    (Closes: #695783)

 -- David Prévot <taffit@debian.org>  Sun, 30 Dec 2012 16:51:23 -0400

muse-el (3.20-2) unstable; urgency=low

  * Fix autoloads file build

 -- Julien Danjou <acid@debian.org>  Wed, 18 Aug 2010 10:27:37 +0200

muse-el (3.20-1) unstable; urgency=low

  * New upstream release.
  * New maintainer.
  * Bump standards version
  * Switch to debcompat 7

 -- Julien Danjou <acid@debian.org>  Sun, 15 Aug 2010 13:49:19 +0200

muse-el (3.12-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Replace the build-dependency tetex-bin by texlive and texlive-latex-extra
    (Closes: #562385)
  * Add emacs23 in {build-,}dependencies (emacs22 is to be removed)

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 05 Feb 2010 18:17:55 +0100

muse-el (3.12-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Update copyright years, download location.
  * debian/rules: Add ChangeLog.04.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Tue, 29 Jan 2008 18:05:58 -0500

muse-el (3.11-3) unstable; urgency=low

  * debian/control:
    - (Build-Depends-Indep): Add emacs22 as an option, since the emacs
      package is not installed by default when emacs22 is installed, like
      I earlier thought it was.
    - Update to Standards-Version 3.7.3.
  * debian/NEWS: Fix malformed entries.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Wed, 09 Jan 2008 15:46:29 -0500

muse-el (3.11-2) unstable; urgency=low

  * Bug fix: 'muse-el: FTBFS: "emacs: command not found"; with xemacs21',
    thanks to Tatsuya Kinoshita (Closes: #439547).
  * debian/control:
    - (Build-Depends-Indep): Change to emacs | emacs21.  I remember not
      being able to but emacsen first due to some build software not
      liking it, but hopefully "emacs" is different.
    - (Depends): Remove the version part of the xemacs21 dependency.
  * debian/rules:
    - (binary-indep): Move all rules from binary-arch to here, since we
      don't build anything that is architecture-dependent.  I'm not
      entirely sure that this is needed, but it can't hurt anything since
      both rules are called by the "binary" target.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Tue, 28 Aug 2007 22:41:54 -0400

muse-el (3.11-1) unstable; urgency=low

  * New upstream release.
  * Bug fix: "#title tag and lisp code evaluation order", thanks to
    Junichi Uekawa (Closes: #436510).
  * debian/control:
    - (Build-Depends-Indep, Depends): Replace emacs21 with emacs22.
  * debian/copyright:
    - Add new contributors.
    - Update license to GPLv3.
  * debian/emacsen-install:
    - Use symlinks in a better way.
  * debian/muse-el.dirs:
    - Add contrib/blosxom.
  * debian/rules:
    - (clean): Fix warning by not ignoring errors in "make distclean"
      invocation.
    - (build): Don't build autoloads.  They come with Muse.
    - (install): Add contrib/blosxom.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Fri, 24 Aug 2007 23:32:22 -0400

muse-el (3.03-1) unstable; urgency=low

  * New upstream release.
  * Remove the manual, since it has been relicensed to the GFDL.  The
    manual is still available online.
  * README.Debian: New file that explains how to get the manual and access
    the QuickStart guide.
  * New emacs-wiki migration guide is installed at
    /usr/share/doc/muse-el/emacs-wiki-migration.txt.
  * debian/control:
    - Add emacsen-common >= 1.4.14, since we are using
      debian-pkg-add-load-path-item.
    - Make it possible to build using texlive rather than tetex-bin.
  * debian/emacsen-startup: Drop fboundp test.  Do the right thing if the
    package has been removed but not purged.
  * debian/rules;
    - Install contrib/make-blog to
      /usr/share/doc/muse-e/contrib/pyblosxom.  Thanks to Sebastian
      P. Luque for noticing.
    - Install QuickStart guide and source.
    - Install muse.rnc, Muse's Relax-NG schema.
    - Install publish-project to examples/.
    - Don't compress some files.
  * Bug fix: "muse-el: Doesn't escape specials when publish using the
    latex style", thanks to Jeremy Hankins (Closes: #340620).
  * Bug fix: "planner-el: 'C-c C-p' chokes", thanks to LI Daobing (Closes:
    #350515).

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Mon, 25 Jun 2007 08:14:00 -0400

muse-el (3.02.8-1) unstable; urgency=low

  * New upstream release.
  * Bug fix: "muse-el: Can not activate planner-mode on Emacs 22", thanks
    to intrigeri (Closes: #391408).
  * Bug fix: "muse-el: muse-http.el is not functional", thanks to Junichi
    Uekawa (Closes: #357949).
  * control (Build-Depends): New field which contains debhelper
    dependency.
    (Standards-Version): Bump to 3.7.2.
  * debian/emacsen-install: Compile files in contrib directory in addition
    to the normal fare.  Thanks to Junichi Uekawa for the suggestion.
  * debian/emacsen-startup: Add contrib directory to load-path.
  * debian/emacsen-remove: Fix comment.
  * debian/compat: New file that sets the debhelper compatibility level to
    4.
  * debian/rules: Remove DH_COMPAT line.  Thanks to Romain Francoise for
    the suggestion.
  * debian/NEWS: Rename from NEWS.Debian.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Tue, 10 Oct 2006 21:44:42 -0400

muse-el (3.02.6-2) unstable; urgency=low

  * Update copyright data.
  * Bump `muse-version' up to 3.02.6.  Thanks to Romain Francoise for
    spotting this.
  * debian/emacsen-install, debian/emacsen-statup: Use autoload file.
  * debian/rules: (build-stamp): Make autoloads.
    (install): Install NEWS properly.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Mon, 16 Jan 2006 21:28:57 -0500

muse-el (3.02.6-1) unstable; urgency=low

  * New upstream release.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Thu, 12 Jan 2006 22:22:01 -0500

muse-el (3.02.5-1) unstable; urgency=low

  * New upstream release.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sat, 17 Dec 2005 12:11:27 -0500

muse-el (3.02.02-1) unstable; urgency=low

  * New upstream release.
  * debian/rules: Change dh_compat to v4, since the new version of
    debhelper won't hit testing for a while, apparently.
  * debian/control: Bump build-depends to debhelper 4.0.0.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sat, 19 Nov 2005 00:50:30 -0500

muse-el (3.02.01.arch.265-1) unstable; urgency=low

  * New Arch snapshot.
  * debian/rules: Update dh_compat to v5.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sun,  6 Nov 2005 19:08:38 -0500

muse-el (3.02.01-1) unstable; urgency=low

  * New upstream release.
  * If you have used the customize interface to save `muse-project-alist',
    you will need to follow the instructions in NEWS.Debian.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Mon, 26 Sep 2005 17:00:46 -0500

muse-el (3.02-1) unstable; urgency=medium

  * New upstream release.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Fri, 16 Sep 2005 01:58:36 -0500

muse-el (3.01.arch.201-4) unstable; urgency=low

  * Makefile: Correct a build issue involving revisions and accidentally
    modifying the original tarball.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Thu, 18 Aug 2005 01:36:27 -0500

muse-el (3.01.arch.201-3) unstable; urgency=low

  * debian/control: Add emacs21 | emacsen to Build-Depends-Indep.  This
    should fix a potential build failure.  We need a binary named `emacs'
    at build time.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sun, 14 Aug 2005 16:28:34 -0500

muse-el (3.01.arch.201-2) unstable; urgency=low

  * debian/control: Shorten description and make first line match the ITP.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sun, 14 Aug 2005 13:45:49 -0500

muse-el (3.01.arch.201-1) unstable; urgency=low

  * New Arch snapshot.
  * Initial Debian upload (Closes: #323020).

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sun, 14 Aug 2005 02:02:03 -0500

muse-el (3.01.arch.195-1) unstable; urgency=low

  * New Arch snapshot.
  * debian/rules (clean): Call $(MAKE) realclean instead of $(MAKE) clean.
  * Attempt to ensure that the full set of changes is generated.
    Thanks to Romain Francoise for the advice.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Tue,  9 Aug 2005 16:40:40 -0500

muse-el (3.01.arch.188-1) unstable; urgency=low

  * New Arch snapshot.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Wed, 27 Jul 2005 02:23:56 -0500

muse-el (3.01.arch.160-1) unstable; urgency=low

  * New Arch snapshot.
  * debian/control: Add tetex-bin to Build-Depends-Indep.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Tue, 19 Jul 2005 02:16:25 -0500

muse-el (3.01.arch.152-1) unstable; urgency=low

  * New Arch snapshot.
  * debian/control: Expand description.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sun, 17 Jul 2005 21:57:34 -0500

muse-el (3.01.arch.150-1) unstable; urgency=low

  * New Arch snapshot.

 -- Michael W. Olson (GNU address) <mwolson@gnu.org>  Sun, 17 Jul 2005 16:04:06 -0500

muse-el (3.01-1) unstable; urgency=low

  * Initial Release.

 -- Trent Buck <trentbuck@gmail.com>  Thu, 23 Jun 2005 13:03:38 +1000
